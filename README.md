# Maven Settings

Bootstrap script that sets up a minimal maven environment for use with Atlassian

Large chunks of this script were copied from the atlassian-checkout project at
https://bitbucket.org/robertmassaioli/atlassian-checkout. It has been tweaked to be used
independently and select different proxies based on fqdn.

It assumes maven is on your path. To use:

    git clone https://bitbucket.org/atlassian/maven-settings.git
    chmod a+x maven-settings/maven-settings.sh
    cd maven-settings && ./maven-settings.sh

in your home directory
