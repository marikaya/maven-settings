#!/bin/bash

# Bootstrap script that sets up a minimal maven environment for use with Atlassian
#
# Large chunks of this script were copied from the atlassian-checkout project at
# https://bitbucket.org/robertmassaioli/atlassian-checkout. It has been tweaked to be used
# independently and select different proxies based on fqdn.
#
# It assumes maven is on your path


function determine_proxy_url() {
    # Based on http://serverfault.com/a/367682
    fqdn=$(host -TtA $(hostname -s)|grep "has address"|awk '{print $1}')
    if [[ "${fqdn}" == "" ]]
    then
       fqdn=$(hostname -s)
    fi

    case $fqdn in
        *buildeng.atlassian.com ) echo "http://m2proxy.buildeng.atlassian.com:8081/content/groups/internal";;
        *private.atlassian.com  ) echo "http://m2proxy-int.private.atlassian.com/content/groups/internal";;
        *syd.atlassian.com      ) echo "https://m2proxy.atlassian.com/content/groups/internal";;
        *                       ) echo "https://maven.atlassian.com/content/groups/internal";;
    esac
}

function maven-settings_install() {
    mkdir -p "$HOME/.m2"
    mvnUserSettingsFile="$HOME/.m2/settings.xml"
    m2CheckoutDir="${initDir}/maven2settings"
    if [ -d "$m2CheckoutDir" ]
    then
        rm -fr "$m2CheckoutDir"
    fi

    echo "We need your Crowd Username and Password to setup maven correctly."
    echo -n "Crowd Username> "
    read crowdUsername
    echo -n "Crowd Password> "
    read -s crowdPassword
    echo ""
    echo -n "Choose a Maven Master Password> "
    read -s mavenMasterPassword
    cp "settings.xml.developer" "$mvnUserSettingsFile"

    echo "Encrypting your master password...."
    masterPassword=`mvn --encrypt-master-password $mavenMasterPassword`
    settingsFile="$HOME/.m2/settings-security.xml"
    echo "<settingsSecurity>" > "$settingsFile"
    echo "<master>$masterPassword</master>" >> "$settingsFile"
    echo "</settingsSecurity>" >> "$settingsFile"
    echo -n "Encrypting your password..."
    crowdPassword=`mvn --encrypt-password "$crowdPassword"`
    echo "SUCCESS"
    echo "Encrypted password is: $crowdPassword"
    echo "Putting encrypted password into: $mvnUserSettingsFile"
    sed -ibackup "s_yourusername_${crowdUsername}_" "$mvnUserSettingsFile"
    sed -ibackup "s_yourpassword_${crowdPassword}_" "$mvnUserSettingsFile"

    proxy_url=$(determine_proxy_url)
    sed -r -ibackup "s_https://m2proxy.atlassian.com/(content/groups|repository)/internal_${proxy_url}_" "$mvnUserSettingsFile"

    echo "Finished running the maven settings bootstrap."
}

initDir=`pwd`
maven-settings_install
